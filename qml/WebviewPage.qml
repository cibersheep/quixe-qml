import QtQuick 2.9
import Ubuntu.Components 1.3
import Morph.Web 0.1
import QtWebEngine 1.7

import "components"

Page {
    id: web
    anchors.fill: parent
    property string game

    header: AppHeader {
        title: "Quixe"
        id: header
    }

    Component.onCompleted: webview.forceActiveFocus()

    WebContext {
        id: webcontext
        userAgent: 'Mozilla/5.0 (Linux; Ubuntu; Linux arm; rv:65.0) Gecko/20100101 Firefox/65.0'
        offTheRecord: false
    }

    WebView {
        id: webview
        context: webcontext
        url: Qt.resolvedUrl('www/play.html?story=' + game)
        focus: true

        anchors {
            fill: parent
            topMargin: header.visible ? header.height : 0//test
        }

        settings {
            javascriptCanAccessClipboard: true
            localContentCanAccessFileUrls: true
            localContentCanAccessRemoteUrls: true
            allowRunningInsecureContent: true
            allowWindowActivationFromJavaScript : true
        }

        Component.onCompleted: {
            settings.localStorageEnabled = true;
        }

        zoomFactor: root.zoomFactor
    }

    ProgressBar {
        height: units.dp(3)

        anchors {
            left: visible ? parent.left  : undefined
            right: visible ? parent.right : undefined
            top: visible
                ? header.visible ? header.bottom : parent.top
                : undefined
        }

        showProgressPercentage: false
        value: (webview.loadProgress / 100)
        visible: (webview.loading && !webview.lastLoadStopped)
    }

    /*This doesn't help yet
     * for OSK hidding when sending a command


    Connections {
        target: Qt.inputMethod

        onVisibleChanged: {
            if (!Qt.inputMethod.visible) {
                webview.focus = false
                webview.focus = true
            }
        }
    }
    */

    HideHeader { }
}
