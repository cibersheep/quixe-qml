import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Morph.Web 0.1
import QtWebEngine 1.7
import Ubuntu.DownloadManager 1.2

import Backend 1.0
import "components"

Page {
    id: webArchive
    property string game
    property string fileName

    anchors {
        fill: parent
        bottom: parent.bottom
    }

    header: AppHeader {
        title: "If Archive"
        id: headerIf
    }

    SingleDownload {
        id: downloadGame

        metadata: Metadata {
            showInIndicator: true
            title: fileName
        }

        onFinished: {
            console.log("FINISHED",path)

            console.log("Path is: " + path, "fileName",fileName)
            var gameName = fileName
            console.log("Copy: ", gameName)
            Backend.copyLocally(path.replace("file://",""), gameName)

            mainPageStack.pop()
        }

        onErrorMessageChanged: {
            console.log("SingleDownload error", errorMessage)
            PopupUtils.open(errorDialog, root.mainView, { "message" : errorMessage })
        }
    }

    WebContext {
        id: webcontextIF
        //userAgent: 'Mozilla/5.0 (Linux; Android 5.0; Nexus 5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.102 Mobile Safari/537.36 Ubuntu Touch Webapp'

        userScripts: [
            WebEngineScript {
                id: cssinjection
                injectionPoint: WebEngineScript.DocumentCreation
                sourceUrl: Qt.resolvedUrl('js/cssinjection.js')
                worldId: WebEngineScript.MainWorld
            }
        ]

        onDownloadRequested: {
            console.log('download requested', download.url, download.downloadFileName);
            console.log("downloadDirectory",download.downloadDirectory)

            PopupUtils.open(downloadingDialog, root.mainView, { "fileName" : download.downloadFileName }) //path })
            webArchive.fileName = download.downloadFileName || i18n.tr("unknown name")
            downloadGame.download(download.url)
        }
    }

    WebView {
        id: webview
        context: webcontextIF
        url: 'http://www.ifarchive.org/indexes/if-archiveXgamesXglulx.html'

        anchors {
            fill: parent
            topMargin: headerIf.visible ? headerIf.height : 0
        }

        settings {
            javascriptCanAccessClipboard: true
            localContentCanAccessFileUrls: true
            localContentCanAccessRemoteUrls: true
            allowRunningInsecureContent: true
            allowWindowActivationFromJavaScript : true
        }

        Component.onCompleted: {
            settings.localStorageEnabled = true;
        }

        zoomFactor: root.zoomFactor
    }

    ProgressBar {
        height: units.dp(3)

        anchors {
            left: visible ? parent.left : undefined
            right: visible ? parent.right : undefined
            top: visible
                ? headerIf.visible ? headerIf.bottom : parent.top
                : undefined
        }

        showProgressPercentage: false
        value: (webview.loadProgress / 100)
        visible: (webview.loading && !webview.lastLoadStopped)
    }

    HideHeader { }

    Component {
        id: downloadingDialog

        DownloadingDialog {
            anchors.fill: parent

            onAbortDownload: downloadGame.cancel()

            percentage: downloadGame.progress
        }
    }

    Component {
        id: errorDialog

        ErrorDialog {
            anchors.fill: parent
        }
    }
}
