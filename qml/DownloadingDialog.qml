/*
 * Copyright (C) 2013 Canonical Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Arto Jalkanen <ajalkane@gmail.com>
 */
import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Dialog {
    id: downloadDialog

    signal abortDownload()
    property Page page
    property string fileName
    property alias percentage: downloadBar.value
    property string dialogTitle: i18n.tr("Download in progress")
    property string descriptionPrepend: i18n.tr("The game %1 will be added to the list of games").arg(fileName)

    title: dialogTitle
    text: descriptionPrepend

    ProgressBar {
        id: downloadBar
        height: units.dp(3)
        anchors {
            left: parent.left
            right: parent.right
        }

        showProgressPercentage: false
        minimumValue: 0
        maximumValue: 100
    }

    Button {
        id: cancelButton
        visible: true
        text: i18n.tr("Cancel")
        onClicked: {
            console.log("Aborting")
            abortDownload()
            console.log("Closing popup")
            mainPageStack.pop()
        }
    }
}
