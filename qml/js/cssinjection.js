function handler(event) {
    console.log('Injecting ubuntu touch styling fixes');

    document.body.setAttribute("id", "body");

    var style = document.createElement('style');
    style.type = 'text/css';
    style.appendChild(document.createTextNode(
        ".Page { margin: 0 !important; background-color: #fdfdfd !important; color: #333333 !important; border-right: 0 !important; padding: 0.1em 1em 4em !important; }" +
        "h1 { color: #0e8cba !important; font-size: 2.5em !important; font-family: sans-serif !important; }" +
        "a, a:hover, #indexpage .Header a { color: #0e8cba !important; }" +
        "#indexpage .ParentLinks a { color: #d0d0d0 !important; }"
    ));

    document.head.appendChild(style);
}

window.addEventListener('load', handler, false);

